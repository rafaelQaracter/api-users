
package com.qaracter.microservice.users.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qaracter.microservice.users.validators.Dni;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase de dominio Transfer Object. Estas son clases de las que se espera tener
 * una gran cantidad de objetos creados a partir de estas. Es decir mi sistema
 * podra crear cientos o miles de objetos User.
 * 
 * @author Rafael Benedettelli
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

	/**
	 * Json ignore excluye esta propiedad de la respuesta json.
	 */
	@JsonIgnore
	private Long id;

	private String name;

	/**
	 * Min es un validador de Spring para obligar a que la edad minima sea de 15
	 * años Max lo mismo, obliga a que sea menor a 60.
	 */
	@Min(15)
	@Max(60)
	private int age;

	/**
	 * De esta manera obligo a que el country tenga un minimo y maximo de dos
	 * letras. Es decir dos letras exactamente. Me sirve para obligar a que provean
	 * un pais en formato ISO.
	 */
	@Size(min = 2, max = 2)
	private String country;

	private double iva;

	private String address;

	private String city;

	@Dni
	private String dni;

}
