package com.qaracter.microservice.users.controller;

import java.util.List;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.qaracter.microservice.users.businessrule.UserService;
import com.qaracter.microservice.users.model.User;

/**
 * ==================================== Capa Controller
 * ====================================
 * 
 * Las clases de esta capa solo tienen la responsabilidad de exponer endpoints
 * para entregar y recibir datos.
 * 
 * No debe agregarse en esta capa logica de negocio o persistencia de datos.
 * 
 * @author Rafael Benedettelli
 */

@Controller
public class UserControllerRest {

	/**
	 * Esta es una referencia a la interface UserService. Con autowired le indicamos
	 * a Spring que nos instancie automaticamente una clase que implemente de
	 * UserService (Como UserServiceImpl) y la asigne aqui.
	 * 
	 * Spring tomara aquella subclase que este anotada con la anotacion @Service
	 * 
	 * Lo que hace Spring automaticamente por nosotros es UserService userService =
	 * new UserServiceImpl();
	 * 
	 */
	@Autowired
	private UserService userService;

	/**
	 * LOGGER TRACING
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserControllerRest.class);

	/**
	 * Este metodo se expone como un endpoint rest en el path
	 * http://localhost:8080/api/rest/users
	 * 
	 * Este endpoint espera recibir datos en formato JSON. Cuando la informacino
	 * llega, automaticamente se transforma de json a java y ya disponemos del
	 * objeto user con toda la informacion cargada para accederla mediante sus
	 * metodos getters.
	 * 
	 * ResponseEntity es un objeto especial que wrappea a los objetos que realmente
	 * queremos retornar. Este tipo de objeto lo usa spring para transformar la
	 * informacion que queremos retornar como un objeto json y agregarle un codigo
	 * de estado de respuesta valido HTTP.
	 * 
	 * La anotacion Post Mapping indica que se utilizara el metodo POST de HTTP.
	 * 
	 * La anotacion @RequestBody es la que indica a spring que llegara un peticion
	 * en el "body" del mensaje http. La anotacion @Valid indica a Spring que tiene
	 * que validar el objeto de acuerdo a las anotacion de validacion dentro de la
	 * clase User (ej @min(12))
	 * 
	 * @param user recibe un usuario ya en java
	 * @return Un ResponseEntity con un mensaje string
	 */
	@PostMapping(path = "api/rest/users", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createUser(@RequestBody @Valid User user) {

		
		LOGGER.info("Creando usuario=========  ");
		
		LOGGER.debug("usuario data " + user.toString());

		// Delegando el procesamiento de user a la capa bussinesrule.
		// Transferimos el Data Transfer Object User a la capa user service para que
		// atienda esta solicitud
		userService.createUser(user);

		// Un mensaje en string mas un Codigo de estado HTTP (CREATED).
		return new ResponseEntity<String>("ok", HttpStatus.CREATED);
	}

	/**
	 * La anotacion GetMapping indica que este metodo se va a mapear como un
	 * endpoint rest via el metodo HTTP GET (que se utiliza cuando lo que
	 * necesitamos es retornar informacion)
	 * 
	 * Este metodo se expone como un endpoint rest en el path
	 * http://localhost:8080/api/rest/users
	 * 
	 * @return un ResponseEntity con codigo de estado HTTP OK (200) y el array de
	 *         usuarios
	 */
	@GetMapping(path = "api/rest/users")
	public ResponseEntity<List<User>> getAllUsers() {

		// Recupera el array de usuarios desde la capa bussines rule
		List<User> users = userService.retrieveAllUsers();

		// Retorna el objeto al cliente REST que lo solicita con codigo de estado HTTP
		// OK (200)
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

	/**
	 * La anotacion GetMapping indica que este metodo se va a mapear como un
	 * endpoint rest via el metodo HTTP GET (que se utiliza cuando lo que
	 * necesitamos es retornar informacion)
	 * 
	 * Este metodo se expone como un endpoint rest en el path
	 * http://localhost:8080/api/rest/users
	 * 
	 * @return un ResponseEntity con codigo de estado HTTP OK (200) y el usuario Dto
	 */
	@GetMapping(path = "api/rest/users/{idUser}")
	public ResponseEntity<User> getUserById(@PathVariable Long idUser) {

		// Recupera el array de usuarios desde la capa bussines rule
		User user = userService.retrieveUserById(idUser);

		// Retorna el objeto al cliente REST que lo solicita con codigo de estado HTTP
		// OK (200)
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/**
	 * La anotacion DeleteMapping indica que este metodo se va a mapear como un
	 * endpoint rest via el metodo HTTP DELETE se utiliza cuando lo que necesitamos
	 * es eliminar informacion)
	 * 
	 * @param userId el id del usuario que se desea eliminar.
	 * @return booleano indicando si el usuario se pudo eliminar o no.
	 */
	@DeleteMapping(path = "api/rest/users/{userId}")
	public ResponseEntity<Boolean> deleteUserById(@PathVariable Long userId) {

		userService.deleteUserById(userId);

		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	/**
	 * La anotacion PutMapping indica que este metodo se va a mapear como un
	 * endpoint rest via el metodo HTTP PUT se utiliza cuando lo que necesitamos es
	 * actualizar informacion)
	 *
	 * ResponseEntity es un objeto especial que wrappea a los objetos que realmente
	 * queremos retornar. Este tipo de objeto lo usa spring para transformar la
	 * informacion que queremos retornar como un objeto json y agregarle un codigo
	 * de estado de respuesta valido HTTP.
	 * 
	 * La anotacion @RequestBody es la que indica a spring que llegara un peticion
	 * en el "body" del mensaje http. La anotacion @Valid indica a Spring que tiene
	 * que validar el objeto de acuerdo a las anotacion de validacion dentro de la
	 * clase User (ej @min(12))
	 * 
	 * @param user recibe un usuario ya en java
	 * @return Un ResponseEntity con un mensaje string
	 */
	@RequestMapping(path = "api/rest/users", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> updateUser(@RequestBody @Valid User user) {

		User updateUser = userService.updateUser(user);

		return new ResponseEntity<User>(updateUser, HttpStatus.OK);
	}
	


}
