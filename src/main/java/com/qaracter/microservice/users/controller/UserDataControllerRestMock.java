package com.qaracter.microservice.users.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class UserDataControllerRestMock {

	@GetMapping(path = "api/rest/users/data/{idUser}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getUserById(@PathVariable Long idUser) {

		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\"id\":1,");
		sb.append("\"address\":\"Calle Princesa 16\", ");
		sb.append("\"city\":\"Madrid\" ");
		sb.append("}");

		return new ResponseEntity<String>(sb.toString(), HttpStatus.OK);
	}
}
