package com.qaracter.microservice.users.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDataClientImpl {

	@Autowired
	UserDataClient userClient;

	public Data retriveUserDataById(Long id) {

		Data data = userClient.findById(String.valueOf(id));

		return data;

	}

}
