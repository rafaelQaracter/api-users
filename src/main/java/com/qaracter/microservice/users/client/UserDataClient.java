package com.qaracter.microservice.users.client;

import org.springframework.cloud.openfeign.FeignClient;

import feign.Param;
import feign.RequestLine;

@FeignClient(value = "jplaceholder", url = "http://127.0.0.1:8080/api/rest/users")
public interface UserDataClient {

	@RequestLine("GET /data/{id}")
	public Data findById(@Param("id") String id);

}
