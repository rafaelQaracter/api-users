package com.qaracter.microservice.users.client;

import org.springframework.context.annotation.Bean;

import feign.Contract;

@org.springframework.context.annotation.Configuration
public class Configuration {
	
	@Bean
	public Contract feignContract() {
		return new Contract.Default();
	}

}
