package com.qaracter.microservice.users.businessrule;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qaracter.microservice.users.client.Data;
import com.qaracter.microservice.users.client.UserDataClientImpl;
import com.qaracter.microservice.users.dao.UserDAO;
import com.qaracter.microservice.users.entities.UserEntity;
import com.qaracter.microservice.users.mappers.UserMapper;
import com.qaracter.microservice.users.model.User;

/**
 * ==================================== Capa Business
 * ====================================
 * 
 * Las clases de esta capa solo tienen la responsabilidad de implementar reglas
 * de negocio. Es decir tratar con ciertas logicas exigidas por la naturaleza
 * del negocio de la aplicacion.
 * 
 * No debe agregarse en esta capa logica de persistencia de datos.
 * 
 * A fines educativos dejamos la persistencia en esta capa hasta que veamos la
 * capa DAO.
 * 
 * La anotacion @Service indica que esta clase debe ser instanciada por spring y
 * debe ser referenciada en todo lugar donde se indique una variable del tipo
 * UserService con la anotacion Autowired.
 * 
 * @Autowired UserService userService;
 * 
 * @author Rafael Benedettelli
 */

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LogManager.getLogger();

	@Autowired
	private UserDAO userDao;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private UserDataClientImpl userDataClient;

	/**
	 * Este metodo es el que se encarga de implementar la logica de negocio cuando
	 * se desea guardar un usuario.
	 */
	public void createUser(User user) {

		LOGGER.info("Persistiendo usuario");

		// Obeteniendo nacionalidad del usuario desde el atributo country a travez de su
		// metodo accesor getter.
		String country = user.getCountry();

		// Utilizando un switch de string para saber la nacionalidad
		// Y en funcion de la misma calcular el IVA
		switch (country) {

		case "AR":
		case "ES":
			user.setIva(21);
			break;

		case "MEX":
			user.setIva(17);
			break;

		default:
			break;
		}

		UserEntity userEntity = new UserEntity();
		userEntity.setName(user.getName());
		userEntity.setAge(user.getAge());
		userEntity.setCountry(user.getCountry());
		userEntity.setIva(user.getIva());

		userDao.save(userEntity);

	}

	/**
	 * Este metodo simplemente retorna todos los usuarios. Si hubiera que
	 * implementar alguna logica adicional o filtro se realizaria aqui.
	 */
	@Override
	public List<User> retrieveAllUsers() {
		
		LOGGER.info("Retrieving all users");

		List<UserEntity> users = userDao.getAll();

		List<User> userDtos = new ArrayList<User>();

		for (UserEntity userEntity : users) {

			User user = new User();
			user.setName(userEntity.getName());
			user.setAge(userEntity.getAge());
			user.setIva(userEntity.getIva());
			user.setCountry(userEntity.getCountry());
			
			LOGGER.debug("user " + user.getName());

			userDtos.add(user);

		}

		return userDtos;
	}

	/**
	 * Este metodo retorna un usuario por id. Para eso usa lo trae usando el metodo
	 * getUserById de la capa dao Como la capa dao solo opera con objetos tipo
	 * entity, posteriormente lo transforma para enviarlo a la capa controller.
	 */
	@Override
	public User retrieveUserById(Long userId) {
		
		LOGGER.info("retrieve user by id " + userId);

		UserEntity userEntity = userDao.getUserById(userId);

		User user = userMapper.as(userEntity);

		Data data = userDataClient.retriveUserDataById(userId);
		
		LOGGER.debug("compose information" + userId);


		// Composing information 
		user.setCity(data.getCity());
		user.setAddress(data.getAddress());

		return user;

	}

	/**
	 * Este metodo simplemente elimina un usuario por id.
	 */
	@Override
	public void deleteUserById(Long userId) {

		userDao.deleteUserById(userId);

	}

	/**
	 * Este metodo actualiza un usuario. En primer lugar tranforma el dto a entity
	 * para enviarlo a la capa dao. Luego cuando recibe el entity lo vuelve a
	 * transformar a dto.
	 * 
	 * Observar que para este tipo de tareas de transformacion se usa la libreria
	 * structmap
	 */
	@Override
	public User updateUser(User user) {

		UserEntity userEntity = new UserEntity();
		userEntity.setAge(user.getAge());
		userEntity.setCountry(user.getCountry());
		userEntity.setName(user.getName());
		userEntity.setIva(user.getIva());

		UserEntity userEntityUpdated = userDao.update(userEntity);

		User userDto = new User();
		userDto.setAge(userEntityUpdated.getAge());
		userDto.setCountry(userEntityUpdated.getCountry());
		userDto.setIva(userEntityUpdated.getIva());
		userDto.setName(userEntityUpdated.getName());

		return userDto;
	}

}
