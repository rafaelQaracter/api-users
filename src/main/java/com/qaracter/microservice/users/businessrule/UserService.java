package com.qaracter.microservice.users.businessrule;

import java.util.List;

import com.qaracter.microservice.users.model.User;

/**
 * Esta interfaz marca un contrato de como deben implementarse las clases
 * Bussines Rule de usuarios.
 * 
 * 
 * @author Rafael Benedettelli
 *
 */
public interface UserService {

	/**
	 * Este es un metodo que solo es declarativo. No tiene implementacion de codigo
	 * real. Es solo para obligar a las clases que lo implementen tener que proveer
	 * una implementacion de estos.
	 * 
	 * Este metodo indica que recibira un User y no retornara nada. Por la firma de
	 * su nombre se entiende que es para crear un usuario.
	 * 
	 * @param user
	 */
	public void createUser(User user);

	/**
	 * Este metodo no recibe ningun parametro y retorna una lista de usuarios.
	 * 
	 * Por su nombre se entiende que es para regresar todos los usuarios del
	 * sistema.
	 * 
	 * @return Lista de usuarios
	 */
	public List<User> retrieveAllUsers();

	/**
	 * Este metodo regresa un User DTO (Data Transfer Object) por id de usuario.
	 * 
	 * @param userId el id del usuario
	 * @return objeto user dto.
	 */
	public User retrieveUserById(Long userId);

	/**
	 * Este metodo elimina un usuario por id de usuario.
	 * 
	 * @param userId el id del usuario
	 * @return objeto user dto.
	 */
	public void deleteUserById(Long userId);

	/**
	 * Este metodo actualiza los datos de un usuario.
	 * 
	 * 
	 * @param user un User DTO
	 * @return El mismo user DTO con los datos actualizados.
	 */
	public User updateUser(User user);

}
