package com.qaracter.microservice.users.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import com.qaracter.microservice.users.entities.UserEntity;
import com.qaracter.microservice.users.model.User;

@Mapper
public interface UserMapper {

	public UserEntity as(User user);

	public User as(UserEntity userEntity);

	public List<User> asDtos(List<UserEntity> userEntity);

	public List<UserEntity> asEntities(List<User> userEntity);

}
