package com.qaracter.microservice.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Esta es clase Entry Point de spring y es la que tiene el metodo main para
 * ejecutar la app.
 * 
 * La anotacino @SpringBootApplication indica que esta es una aplicacion de tipo spring boot.
 * 
 * @author Rafael Benedettelli
 *
 */
@SpringBootApplication
@EnableFeignClients
public class ApiUsersApplication {

	/**
	 * Metodo main
	 * @param args
	 */
	public static void main(String[] args) {
		
		SpringApplication.run(ApiUsersApplication.class, args);
		
	
	}

}
