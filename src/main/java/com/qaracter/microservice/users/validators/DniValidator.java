package com.qaracter.microservice.users.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.qaracter.microservice.users.validators.Dni;

public class DniValidator implements ConstraintValidator<Dni, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		
		
		return value.length() == 9;
	}

}
