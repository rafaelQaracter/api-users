package com.qaracter.microservice.users.validators;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Constraint(validatedBy = DniValidator.class)
@Retention(RUNTIME)
@Target({ FIELD })
public @interface Dni {

	String message() default "Invalid dni";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
