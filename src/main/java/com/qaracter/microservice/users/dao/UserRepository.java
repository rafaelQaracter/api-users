package com.qaracter.microservice.users.dao;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.qaracter.microservice.users.entities.UserEntity;

/**
 * Esta clase extiende de org.springframework.data.repository.Repository que es
 * una interface que me permite implementar metodos que Spring automaticamente
 * identificara como acciones en la base de datos.
 * 
 * 
 * @author Rafael Benedettelli
 *
 */
@org.springframework.stereotype.Repository
public interface UserRepository extends Repository<UserEntity, Long> {

	public UserEntity save(UserEntity entity);

	public UserEntity findById(Long id);

	public List<UserEntity> findAll();

	public Long count();

	public void deleteById(Long id);

}