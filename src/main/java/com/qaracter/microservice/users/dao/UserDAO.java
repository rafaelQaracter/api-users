package com.qaracter.microservice.users.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qaracter.microservice.users.entities.UserEntity;

/**
 * Esta es la capa DAO (Data Access Object) Todas las clases en esta capa tienen
 * la responsabilidad exclusiva de guardar y recuperar objetos del tipo entity
 * desde la base de datos o cualquier otro medio de persistencia.
 * 
 * En esta capa no puede haber logica de negocios o codigo pertinente a resolver
 * problemas de la capa REST.
 * 
 * 
 * @author Rafael Benedettelli
 *
 */
@Repository
public class UserDAO {

	/**
	 * Se inyecta un objeto del tipo UserRepository. La instancia la crea Spring ya
	 * que es spring quien mantiene una implementacion de esta interface
	 * internamente.
	 * 
	 * repository sera quien realmente guarde y rescate info de la bd.
	 * 
	 * Siendo userDao en este caso un delegator o un Wrapper de repository.
	 */
	@Autowired
	private UserRepository repository;

	/**
	 * Guarda un usuario en la bd.
	 * 
	 * @param user Entity
	 */
	public void save(UserEntity user) {

		repository.save(user);

	}

	/**
	 * Retorna todos los usuarios de la bd.
	 * 
	 * @return Lista de UserEntities
	 */
	public List<UserEntity> getAll() {

		return repository.findAll();

	}

	public UserEntity getUserById(Long userId) {

		return repository.findById(userId);
	}

	/**
	 * Elimina un usuario por id.
	 * 
	 * @param el userId
	 * @return true si pudo elimarlo o false en el caso contrario.
	 */
	public void deleteUserById(Long userId) {

		repository.deleteById(userId);
	}

	/**
	 * Actualiza los atributos de un objeto en la base de datos.
	 * 
	 * @param userEntity El userEntity que se desea actualizar con los nuevos
	 *                   valores
	 * @return El userEntity con los valores actualizados.
	 */
	public UserEntity update(UserEntity userEntity) {

		return repository.save(userEntity);
	}

}
