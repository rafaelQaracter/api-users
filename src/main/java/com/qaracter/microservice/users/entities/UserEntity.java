package com.qaracter.microservice.users.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

/**
 * Todos los objetos dentro de este paquete
 * son del tipo Entity y suelen mapearse directamente con tablas en la BD.
 * Este tipo de objetos se usan en la capa dao.
 * 
 * @author Rafael Benedettelli
 * 
 * La anotacion  @Entity convierte a la clase en persistible y crea una tabla con el mismo nombre en la bd.
 *
 */
/**
 * @author expeditive
 *
 */
@Entity
@Data
public class UserEntity {

	/**
	 * La anotacion ID hace que el atributo Long id sea el id unico en la tablsa. La
	 * anotacion GenerateValue implementa en BD una estrategia automatica para la
	 * generacion de ids incrementales.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/**
	 * Sera la columna name en la bd
	 */
	private String name;

	/**
	 * sera la columna age en la bd
	 */
	private int age;

	/**
	 * Sera la columna country en la bd
	 */
	private String country;

	/**
	 * Sera la columna iva en la bd. En este caso, la columna column con el valor
	 * name crea para este atributo excepcionalmente un nombre de columna diferente.
	 */
	@Column(name = "iva_impuesto")
	private double iva;

}
